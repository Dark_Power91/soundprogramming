// WrapperTest.cpp : Defines the entry point for the console application.

#include "stdafx.h"
#include "Mixer.h"
#include "windows.h"
#include "common.h"

const int INTERFACE_UPDATETIME = 50;
const float	VOLUME_STEP = 0.05f;
const float PAN_STEP = 0.05f;

int FMOD_Main()
{
	//Initialize Mixer
	Mixer m;
	m.InitSystem(512);

	//Initialize Interface
	void* extradriverdata = 0;
	Common_Init(&extradriverdata);

	//Load 3 Track (1 stream 2 sound)

	int track = m.Load("../Media/singing.wav", true);
	int track1 = m.Load("../Media/swish.wav" );
	int track2 = m.Load("../Media/jaguar.wav");

	int SelectedTrack = -1;
	bool IsTrackSelected = false;

	//Main Loop
	do
	{
		Common_Update();

		if (IsTrackSelected)
			//Do Action on track
		{
			if (Common_BtnPress(BTN_ACTION1))
			{
				m.Play(SelectedTrack);
			}
			else if (Common_BtnPress(BTN_MORE))
			{
				IsTrackSelected = false;
			}
			else if (m.IsPlaying(SelectedTrack))
			{
				if (Common_BtnPress(BTN_ACTION2))
				{
					m.Pause(SelectedTrack);
				}
				else if (Common_BtnPress(BTN_ACTION3))
				{
					m.Stop(SelectedTrack);
				}
				else if (Common_BtnPress(BTN_ACTION4))
				{
					m.SetLoop(SelectedTrack, true);
				}
				else if (Common_BtnPress(BTN_ACTION5))
				{
					m.SetLoop(SelectedTrack, false);
				}
				else if (Common_BtnPress(BTN_LEFT))
				{
					m.SetPan(SelectedTrack, m.GetPan(SelectedTrack) - PAN_STEP);
				}
				else if (Common_BtnPress(BTN_RIGHT))
				{
					m.SetPan(SelectedTrack, m.GetPan(SelectedTrack) + PAN_STEP);
				}
				else if (Common_BtnPress(BTN_UP))
				{
					m.SetVolume(SelectedTrack, m.GetVolume(SelectedTrack) + VOLUME_STEP);
				}
				else if (Common_BtnPress(BTN_DOWN))
				{
					m.SetVolume(SelectedTrack, m.GetVolume(SelectedTrack) - VOLUME_STEP);
				}
			}

			Common_Draw("===================================================");
			Common_Draw("Wrapper Example.");
			Common_Draw("===================================================");
			Common_Draw("");
			Common_Draw("Press %s to play selected sound", Common_BtnStr(BTN_ACTION1));
			if (m.IsPlaying(SelectedTrack))
			{
				Common_Draw("Press %s to pause selected sound", Common_BtnStr(BTN_ACTION2));
				Common_Draw("Press %s to stop selected sound", Common_BtnStr(BTN_ACTION3));
				Common_Draw("");
				Common_Draw("Press %s to enable looping", Common_BtnStr(BTN_ACTION4));
				Common_Draw("Press %s to disable looping", Common_BtnStr(BTN_ACTION5));
				Common_Draw("");
				Common_Draw("Press %s to move pan to left", Common_BtnStr(BTN_LEFT));
				Common_Draw("Press %s to move pan to right", Common_BtnStr(BTN_RIGHT));
				Common_Draw("");
				Common_Draw("Press %s to turn up volume", Common_BtnStr(BTN_UP));
				Common_Draw("Press %s to turn down volume", Common_BtnStr(BTN_DOWN));

			}
			Common_Draw("");
			Common_Draw("Press %s to go back", Common_BtnStr(BTN_MORE));
			Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));

		}
		else 
		{
			//Select track
			if (Common_BtnPress(BTN_ACTION1))
			{
				SelectedTrack = 0;
				IsTrackSelected = true;
			}
			else if (Common_BtnPress(BTN_ACTION2))
			{
				SelectedTrack = 1;
				IsTrackSelected = true;
			}
			else if (Common_BtnPress(BTN_ACTION3))
			{
				SelectedTrack = 2;
				IsTrackSelected = true;
			}

			Common_Draw("===================================================");
			Common_Draw("Wrapper Example.");
			Common_Draw("===================================================");
			Common_Draw("");
			Common_Draw("Press %s to select singing sound (Loaded as Stream)", Common_BtnStr(BTN_ACTION1));
			Common_Draw("Press %s to select swish sound (Loaded as Sound)", Common_BtnStr(BTN_ACTION2));
			Common_Draw("Press %s to select jaguar sound (Loaded as Sound)", Common_BtnStr(BTN_ACTION3));
			Common_Draw("");
			Common_Draw("Press %s to quit", Common_BtnStr(BTN_QUIT));
			Common_Sleep(INTERFACE_UPDATETIME - 1);
		}

	} while (!Common_BtnPress(BTN_QUIT));
	return 0;
}
