#pragma once
#include "fmod.hpp"
#include "fmod_errors.h"
#include "stdio.h"
#include "stdlib.h"
#include <string>
#include <vector>
#include <algorithm>

class Mixer
{
public:
	Mixer();
	~Mixer();

	void InitSystem(int maxChannels = 512);
	int  Load(const std::string path, const bool isStream = false);
	void Play(unsigned int trackNumber);
	void Pause(unsigned int trackNumber);
	void Stop(unsigned int trackNumber);
	void SetLoop(unsigned int trackNumber, bool isLooping);
	void SetPan(unsigned int trackNumber, float panLevel);
	void SetVolume(unsigned int trackNumber, float volume);
	void ReleaseSound(unsigned int trackNumber);
	bool IsPlaying(unsigned int trackNumber);
	float GetPan(unsigned int trackNumber);
	float GetVolume(unsigned int trackNumber);

private:
	void CheckIndex(unsigned int trackNumber);
	void CheckResult();
	int  AviableIndex();

	FMOD::System* m_system;
	std::vector<FMOD::Sound*> m_sounds;
	std::vector<FMOD::Channel*> m_channels;
	FMOD_RESULT m_result;
};

