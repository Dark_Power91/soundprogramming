#include "stdafx.h"
#include "Mixer.h"

Mixer::Mixer()
{
}

Mixer::~Mixer()
{
	for each (auto s in m_sounds)
	{
		s->release();
	}
	m_system->release();
}

void Mixer::InitSystem(int maxChannels)
{
	m_result = FMOD::System_Create(&m_system);
	CheckResult();
	m_result = m_system->init(maxChannels, FMOD_INIT_NORMAL, 0);
	CheckResult();
}

int Mixer::Load(const std::string path, const bool isStream)
{
	int Index = AviableIndex();
	m_result = isStream ? m_system->createStream(path.c_str(), FMOD_DEFAULT, 0, &m_sounds[Index]):m_system->createSound(path.c_str(), FMOD_DEFAULT, 0, &m_sounds[Index]);
	CheckResult();
	return Index;
}

void Mixer::Play(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	if (m_channels[trackNumber] != nullptr)
	{
		m_result = m_channels[trackNumber]->setPaused(false);
		CheckResult();
	}

	m_result = m_system->playSound(m_sounds[trackNumber], 0, false, &m_channels[trackNumber]);
	CheckResult();

}

void Mixer::Pause(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	m_result = m_channels[trackNumber]->setPaused(true);
	CheckResult();
}

void Mixer::Stop(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	m_result = m_channels[trackNumber]->stop();
	CheckResult();
	m_channels[trackNumber] = nullptr;
}

void Mixer::SetLoop(unsigned int trackNumber, bool isLooping)
{
	CheckIndex(trackNumber);
	m_result = isLooping ? m_sounds[trackNumber]->setMode(FMOD_LOOP_NORMAL) : m_sounds[trackNumber]->setMode(FMOD_LOOP_OFF);
	CheckResult();
}

void Mixer::SetPan(unsigned int trackNumber, float panLevel)
{
	CheckIndex(trackNumber);
	m_result = m_channels[trackNumber]->setPan(std::max(-1.0f, std::min(panLevel, 1.0f)));
	CheckResult();
}

void Mixer::SetVolume(unsigned int trackNumber, float volume)
{
	CheckIndex(trackNumber);
	m_result = m_channels[trackNumber]->setVolume(std::max(0.0f,volume));
	CheckResult();
}

void Mixer::ReleaseSound(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	m_result = m_sounds[trackNumber]->release();
	CheckResult();

	m_sounds[trackNumber] = nullptr;
	m_channels[trackNumber] = nullptr;
}

bool Mixer::IsPlaying(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	bool isPlaying = false;
	m_result = m_channels[trackNumber]->isPlaying(&isPlaying);
	return isPlaying;
}

float Mixer::GetPan(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	FMOD::ChannelControl *cc = m_channels[trackNumber];
	int out, in;
	float mat[32][32], pan;

	m_result = cc->getMixMatrix(0, &out, &in, 32);
	CheckResult();
	m_result = cc->getMixMatrix(&mat[0][0], &out, &in, 32);
	CheckResult();

	// calculate a pan value based on front left/front right.
	float l = 1.0f - (2 * (mat[0][0] * mat[0][0]));
	float r = (2 * (mat[1][0] * mat[1][0]) - 1.0f);

	pan = (l + r) * 0.5f;

	return pan < -1.0f ? -1.0f : pan > 1.0f ? 1.0f : pan;
}

float Mixer::GetVolume(unsigned int trackNumber)
{
	CheckIndex(trackNumber);
	float volume; 
	m_result = m_channels[trackNumber]->getVolume(&volume);
	CheckResult();
	return volume;
}

void Mixer::CheckIndex(unsigned int trackNumber)
{
	if (m_sounds.size() < trackNumber || m_sounds[trackNumber] == nullptr)
	{
		printf("FMOD error! Invalid trakcNumber\n");
		exit(-1);
	}
}

void Mixer::CheckResult()
{
	if (m_result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", m_result, FMOD_ErrorString(m_result));
		exit(-1);
	}
}

int Mixer::AviableIndex()
{
	for (size_t i = 0; i < m_sounds.size(); ++i)
	{
		if (m_sounds[i] == nullptr)
		{
			return i;
		}
	}
	m_sounds.push_back(nullptr);
	m_channels.push_back(nullptr);
	return m_sounds.size() - 1;
}
