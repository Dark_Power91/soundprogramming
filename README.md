Se si è eseguita un installazione delle API di FMOD su directory custom, per poter compilare il codice è necessario cambiare
la directory di inclusione e quella delle librerie. Tutti gli altri Path dovrebbero essere relativi.

le due cartelle standard per FMOD sono:
-Inclusione:
	C:\Program Files (x86)\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\inc
	
-Librerie:
	C:\Program Files (x86)\FMOD SoundSystem\FMOD Studio API Windows\api\lowlevel\lib